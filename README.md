# Labirynt
* Rafał Burczyński
* Michał Stanisz
## Założenia projektowe
### Minimum
* Generowanie labiryntu
* Widok z pierwszej osoby
* Minimapa
### Dodatkowe założenia
* Wskazówki
* Podświetlenie odwiedzonych pól
* Edytor labiryntu
* Ranking
* Czas przejścia
* Punkty (zbierane)
* Ekran początkowy
* Zasady gry
## Podział prac
### Rafał Burczyński
* Generowanie labiryntu
* Minimapa
### Michał Stanisz
* Widok z pierwszej osoby
* Podświetlenie odwiedzonych pól
* Wskazówki
